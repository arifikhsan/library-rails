class BookController < ApplicationController
    skip_before_action :verify_authenticity_token # callback

    def index
        render json: {books: Book.all}
    end

    def new
        Book.create(title: params["title"], detail: params["detail"])
        render json: {status: "success"}
    end

    def one
        book = Book.select(:title, :detail).where(id: params["id"])
        if book.present?
            render json: book
        else
            render json: {message: 'not found'}
        end
    end

    def update
        book = Book.find_by(id: params["id"])
        if book.present?
            book.update(title: params["title"], detail: params["detail"])
            render json: book
        else
            render json: {message: 'not found'}
        end
    end

    def delete
        book = Book.find_by(id: params["id"])
        if book.present?
            book.destroy
            render json: book
        else
            render json: {message: 'not found'}
        end
    end

    private

    def apaajah
        p 'aa'
    end

end
