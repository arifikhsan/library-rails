Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/secretroom', as: 'rails_admin'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/', to: "book#index"
  post '/', to: "book#new"
  get '/:id', to: "book#one"
  put '/:id', to: "book#update"
  delete '/:id', to: "book#delete"

end
